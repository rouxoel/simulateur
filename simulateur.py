from posixpath import split
from random import randint
import comportement

class simulateur():
    def __init__(self):
        self.nombre_de_tour = 100 # Indiquez le nombre de tours
        self.partie = partie()
        self.prepare_replay()
        self.gestion_partie()

    def gestion_partie(self):
        '''Déroule la partie sous forme de tours dans une boucle'''
        self.partie.trajet = comportement.trajet()
        for i in range(self.nombre_de_tour):
            self.partie.new_tour_de_jeu()
            self.tour_en_cours = self.partie.tour_en_cours
            self.partie.trajet.gemmes = self.partie.gemmes
            self.partie.trajet.drone_position = self.partie.drone_position
            self.partie.trajet.decision()
            self.tour_en_cours.execute_le_tour(self.partie.trajet.ordre)
            self.mettre_a_jour_donnees_partie()
            self.ecris_replay()
            if self.partie.gemmes == []:
                self.partie.fin_de_la_partie = True
                break
        self.partie.fin_de_la_partie = True
    
    def mettre_a_jour_donnees_partie(self):
        self.partie.drone_position = self.tour_en_cours.drone_position
        self.partie.gemmes = self.tour_en_cours.gemmes
    
    def prepare_replay(self):
        '''Nettoie le précédent fichier'''
        with open("partie.txt","w") as fichier:
            for gemme in self.partie.gemmes:
                fichier.write(str(gemme[0]) + "\n")
                fichier.write(str(gemme[1]) + "\n")

    def ecris_replay(self):
        '''La partie génère un fichier récapitulant les ordres donnés pour avoir un replay de celle-ci
        le fichier génèré est sous la forme d'un fichier texte, chaque ligne correspond à un ordre'''
        with open("partie.txt","a") as fichier:
            fichier.write(self.tour_en_cours.ordre + "\n")



class partie():
    """Gestionnaire de la partie, traite les entrées qu'on lui donne et renvoi des résultats"""
    def __init__(self):
        self.width = 16
        self.height = 12
        self.gemmes = []
        self.drone_position = (0,0)
        self.tour_de_jeu = 1
        self.pretour()
        self.compteur_cases = 0

    def pretour(self):
        """"Initialise avant le premier tour de jeu, la disposition des gemmes sur la carte"""
        nombre_de_gemmes = 10
        gemmes = []
        for i in range(nombre_de_gemmes):
            gemme = self.new_gemme()
            if gemme not in gemmes:
                gemmes.append(self.new_gemme())
                
        self.gemmes = gemmes
    
    def new_gemme(self):
        """Génère une nouvelle gemme et la place au hasard sur la carte, sauf sur la base"""
        
        x = randint(1,self.width-1)
        y = randint(1,self.height-1)
        return (x,y)

    def new_tour_de_jeu(self):
        """"Lancement d'un nouveau tour de jeu, cette méthode supprime le tour précédent"""
        self.tour_en_cours = tour_de_jeu()
        self.tour_de_jeu += 1
        self.tour_en_cours.tour = self.tour_de_jeu
        self.tour_en_cours.width = self.width
        self.tour_en_cours.height = self.height
        self.tour_en_cours.drone_position = self.drone_position
        self.tour_en_cours.gemmes = self.gemmes

    def fin_de_la_partie(self):
        pass

    def formate_les_donnees(self):
        '''Encode les données sous la forme d'un tableau unique :
        - tableau avec les gemmes
        - position du drone'''
        return [self.gemmes,self.drone_position]


class tour_de_jeu():
    """Correspond à un tour de jeu"""
    def __init__(self):
        self.tour = 0
        self.width = 16
        self.height = 12
        self.fin_de_la_partie = False
        self.drone_position = (0,0)
        self.gemmes = []
    
    def recupere_ordre(self,ordre):
        """Les ordres doivent être sous la forme RAMASSER ou ALLER X Y,
        ils doivent être sous la forme d'une chaine de caractère"""
        #ordre = "ALLER 4 5"
        if self.verifie_ordre(ordre):
            return ordre
        else:
            self.fin_de_la_partie = True
            return "Fin de la partie, le dernier ordre est faux" ###################################################

    def verifie_ordre(self, ordre):
        """Vérifie l'ordre, renvoi True ou False"""
        #Vérifier le type
        if type(ordre) != str:
            return False
        #Vérifier les mots-clés
        mots = ordre.split()
        if mots[0] == "RAMASSER":
            return True
        
        if mots[0] == "ALLER" and len(mots) > 2:
            #Vérifier que les coordonnées sont bien des entiers
            if self.verifier_coordonees(mots):
                x = int(mots[1])
                y =  int(mots[2])
                # Vérifier que les coordonnées sont bien à l'intérieur du périmètre de la carte
                if (x < 0 or y < 0) or (x >= self.width or y >= self.width):
                    return False
                # Renvoi True si toutes les conditions ont été validées
                return True

        return False
    
    def verifier_coordonees(self, mots):
        '''Vérifier que les coordonnées soient bien des entiers'''
        try:
            x = int(mots[1])
            y =  int(mots[2])
            return True

        except ValueError:
            return False
        
    def execute_le_tour(self,ordre):
        ''' Execute l'ordre donné. note : Le drone a besoin d'être sur la case où se trouve l'objet pour le ramasser'''
        self.ordre = self.recupere_ordre(ordre)
        if self.ordre == "RAMASSER":
            self.ramasse_objet()
        elif self.ordre.split()[0] == "ALLER":
            self.avance_le_drone()

    def recupere_coordonnees_ordre(self):
        x_ordre = int(self.ordre.split()[1])
        y_ordre = int(self.ordre.split()[2])

        return (x_ordre,y_ordre)

    def avance_le_drone(self):
        '''Avance le drone d'une case, met à jour sa position'''
        x_actuel,y_actuel = self.drone_position
        x_ordre,y_ordre = self.recupere_coordonnees_ordre() 
        if x_actuel != x_ordre and y_actuel != y_ordre: # déplacement en diagonale
            if x_actuel > x_ordre and y_actuel < y_ordre: #Diagonale haut-gauche
                self.drone_position = (x_actuel - 1, y_actuel + 1)
            elif x_actuel > x_ordre and y_actuel > y_ordre: #Diagonale bas-gauche
                self.drone_position = (x_actuel - 1, y_actuel - 1) 
            elif x_actuel < x_ordre and y_actuel < y_ordre: #Diagonale haut-droit
                self.drone_position = (x_actuel + 1, y_actuel + 1)
            elif x_actuel < x_ordre and y_actuel > y_ordre: #Diagonale bas-droit
                self.drone_position = (x_actuel + 1, y_actuel - 1) 
        elif x_actuel == x_ordre: # Déplacement vertical
            if y_actuel < y_ordre:
                self.drone_position = (x_actuel, y_actuel + 1)
            if y_actuel > y_ordre:
                self.drone_position = (x_actuel, y_actuel - 1)
        elif y_actuel == y_ordre: # Déplacement horizontal
            if x_actuel < x_ordre:
                self.drone_position = (x_actuel + 1, y_actuel)
            if x_actuel > x_ordre:
                self.drone_position = (x_actuel - 1, y_actuel)


    def ramasse_objet(self):
        '''Enlève la gemme ramassée de la liste'''
        del self.gemmes[self.gemmes.index(self.drone_position)]

#simulateur() # Décommenter cette ligne pour faire varier la position des gemmes à chaque partie