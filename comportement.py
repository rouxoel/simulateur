class trajet:
    '''Correspond à la classe qui va gérer le comportement du drone
    à partir des données de l'environnement'''
    def __init__(self):
        self.ordre = "RAMASSER"
        self.drone_position = (0,0)
        self.gemmes = []

    def decision(self):
        '''Prise de décision du drone. Un algorithme naif qui 
        recherche l'objet le plus proche pour aller le ramasser.
        C'est ici que les élèves doivent écrire leur code '''
        x_drone = self.drone_position[0]
        y_drone = self.drone_position[1]
        distance_mini = float("inf")
        for i in range(len(self.gemmes)):
            #Calcule la distance entre le drone et la gemme
            x_gemme = self.gemmes[i][0]
            y_gemme = self.gemmes[i][1]
            distance = ((x_gemme - x_drone)**2+(y_gemme - y_drone)**2)**(1/2)
            if distance < distance_mini:
                distance_mini = distance
                i_gemme = i
        if distance_mini < 1:
            self.ordre = "RAMASSER"
        else :
            self.ordre = "ALLER " + str(self.gemmes[i_gemme][0]) + " " + str(self.gemmes[i_gemme][1])