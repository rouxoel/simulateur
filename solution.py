
# gemmes = [(11,7),(2,8),(8,8),(2,11),(4,1),(9,6),(6,1),(2,6),(4,9),(14,2)] # Score à battre 41
# gemmes = [(5,8),(9,5),(14,8),(15,1),(13,5)] # Score à battre 25
gemmes = [(13,5),(11,7),(10,3),(15,10),(1,1),(1,3),(7,11),(2,11),(12,7),(10,6)] # Score à battre 36
# G = base_graphe(16,12)

import heapdict as hp


## Modélisation du graphe
def base_graphe(nbr_colonnes,nbr_lignes):
    ''' Retourne un graphe sous forme de grille de l lignes et c colonnes'''
    i = 0
    graphe = {}
    for colonne in range(nbr_colonnes):
        for ligne in range(nbr_lignes):
            graphe[i] = quels_voisins(i,nbr_lignes,nbr_colonnes)
            i += 1
    return graphe

def quels_voisins(i, nbr_lignes, nbr_colonnes):
    liste = set()
    # Voisins de la colonne de gauche
    if i // nbr_lignes > 0:
        num_colonne = (i - nbr_lignes) // nbr_lignes
        for j in range(3):
            sommet = i-(nbr_lignes - 1 +j)
            if sommet >= 0 and sommet // nbr_lignes == num_colonne:
                liste.add(sommet)
    # Voisin de la colonne de droite
    if i // nbr_lignes < nbr_colonnes - 1:
        num_colonne = (i + nbr_lignes) // nbr_lignes
        for j in range(3):
            sommet = i+(nbr_lignes - 1 +j)
            if sommet < nbr_lignes * nbr_colonnes and sommet // nbr_lignes == num_colonne:
                liste.add(sommet)
    # Voisin du haut
    sommet = i + 1
    if sommet // nbr_lignes == i // nbr_lignes:
        liste.add(sommet)
    # Voisin du bas
    sommet = i - 1
    if sommet // nbr_lignes == i // nbr_lignes:
        liste.add(sommet)

    return liste

def vers_coordonnees(num_sommet, nbr_lignes):
    x = num_sommet // nbr_lignes
    y = num_sommet - (nbr_lignes * x)

    return x,y

def de_coordonnees(coords, nbr_lignes):
    x,y = coords
    return x * nbr_lignes + y


## Recherche d'itinéraire
def dijkstra(source,G):
    parents = {}
    #On initialise un tableau dist et la file de priorité
    dist = {s:float("inf") for s in G}
    dist[source] = 0
    file_p = hp.heapdict()
    file_p[source] = 0 # 0 correspond à dist[source]
    parents[source] = None
    poids_uv = 1
    while file_p:
        u, dist_u = file_p.popitem() # Récupère le sommet avec la distance la plus faible
        for v in G[u]:
            # Relaxer les voisins
            w_dist = dist_u + poids_uv
            if dist[v] > w_dist:
                dist[v] = w_dist
                file_p[v] = w_dist
                parents[v] = u
    return dist,parents

# resultats = dijkstra(0,G)

def table_routage(gemmes,nbr_colonnes,nbr_lignes):
    graphe = base_graphe(nbr_colonnes,nbr_lignes)
    resultats = {}
    drone = de_coordonnees((0,0), nbr_lignes)
    resultats[drone] = dijkstra(drone,graphe)
    for gemme in gemmes:
        source = de_coordonnees(gemme,12)
        resultats[source] = dijkstra(source,graphe)
    return resultats

# table = table_routage(gemmes,16,12)

# ---------------------------------------#
# ce code provient de ce site : https://python.jpvweb.com/python/mesrecettespython/doku.php?id=permutations
def permutliste(seq, er=False):
    """Retourne la liste de toutes les permutations de la liste seq (non récursif).
       si er=True: élimination des répétitions quand seq en a (ex: [1,2,2]
    """
    p = [seq]
    n = len(seq)
    for k in range(0,n-1):
        for i in range(0, len(p)):
            z = p[i][:]
            for c in range(0,n-k-1):
                z.append(z.pop(k))
                if er==False or (z not in p):
                    p.append(z[:])
    return p
# ---------------------------------------#


def itineraire(gemmes,nbr_colonnes,nbr_lignes):
    table = table_routage(gemmes,nbr_colonnes,nbr_lignes)
    permutations = permutliste(gemmes)
    cout_mini = float("inf")
    itineraire = []
    for permutation in permutations:
        cout = table[0][0][de_coordonnees(permutation[0],nbr_lignes)]
        for i in range(len(permutation)-1):
            depart = de_coordonnees(permutation[i],nbr_lignes)
            arrivee = de_coordonnees(permutation[i+1],nbr_lignes)
            cout = cout + table[depart][0][arrivee]
        if cout < cout_mini:
            cout_mini = cout
            itineraire = permutation

    return cout_mini, itineraire





















