from cProfile import label
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.graphics import Line, Rectangle, Color
from kivy.config import Config
from kivy.core.window import Window
from sprite import *
from kivy.clock import Clock
from random import *
from simulateur import *

class map(Widget):
    def __init__(self, **kwargs):
        super(map, self).__init__(**kwargs)
        self.type = "map"
        self.showMap()

    def showMap(self):
        with self.canvas:
            Rectangle(pos=self.pos, size=Window._get_size(),source="ressources/World_Map.png")

class popup_fin(Widget):
    def __init__(self, **kwargs):
        super(popup_fin, self).__init__(**kwargs)
        self.type = "compteur"
        self.label = Label(text="coucou")
        self.label.pos = (350,0)

class plateau_de_jeu(Widget):
    """Constitue la fenêtre rassemblant tous les éléments graphiques"""
    def __init__(self, **kwargs):
        super(plateau_de_jeu, self).__init__(**kwargs)
        self.gemmes = []
        self.drones = 1
        self.type = "plateau_de_jeu"
        self.liste_des_elements = []
        #print([type(widget) for widget in self.walk(loopback=True)])

    def assemble_les_elements(self):

        self.add_widget(map())
        self.liste_des_elements.append("map")
        i = 0
        #print(self.gemmes)
        for x,y in self.gemmes:
            nouvelle_gemme = gemme()
            nouvelle_gemme.setPos(x,y)
            self.add_widget(nouvelle_gemme)
            self.liste_des_elements.append("gemme"+str(i))
            i += 1
        
        for i in range(self.drones):
            self.add_widget(drone())
            self.liste_des_elements.append("drone"+str(i))
        
        label = popup_fin()
        self.add_widget(label.label)
    
        #print([type(widget) for widget in self.walk(loopback=True)])


class simulateurApp(App):
    def __init__(self, **kwargs):
        super(simulateurApp, self).__init__(**kwargs)
        self.partie = partie()
        self.replay_tours = []
        self.tour = 0
        self.frames = 0
        self.ordre_pos = (-1,-1)
        self.precision = 15 # Précision pour déterminer si le drone est au dessus des gemmes ou non
        #self.tour_suivant()
        self.lis_fichier_partie()
        self.debutHorloge()  # A commenté si on veut juste avoir un visuel sur le plateau et la répartition des gemmes et du drone
        

    def debutHorloge(self):
        """Débute la prise en compte des intervalles en seconde. Peut également s'écrire : on_start() - 
        cela permet de lancer automatiquement la fonction sans la convoquer dans le constructeur"""
        Clock.schedule_interval(self.tour_suivant, .075)
    
    def tour_suivant(self, *args):
        """Est reliée à la pendule et est appelée périodiquement, correspond à un tour de jeu"""
        #self.moveDrone()
        if self.tour < len(self.replay_tours):
            self.replay()
        #self.drone.goTo(4,7)
        #print(self.drone.convertGrid())

    def replay(self):
        '''Exécute l'ordre du fichier de replay en fonction de la ligne demandée'''
        ordre = self.replay_tours[self.tour]
        ordre = ordre.split()
        # Un tour est ajouté tous les 5 frames environ ?
        if ordre[0] == "RAMASSER":
            self.enleve_gemme()
            self.tour += 1
            self.frames = 0
        else:
            self.ordre_pos = (int(ordre[1]),int(ordre[2]))
            self.avance_le_drone(int(ordre[1]),int(ordre[2]))
            if self.frames == 2 and not self.est_au_dessus(self.drone.pos,self.ordre_pos): # Incremente le compteur toutes les 5 frames (correspond à un tour)
                self.partie.compteur_cases += 1
                self.compteur.text = "Nombre de cases parcourues : " + str(self.partie.compteur_cases)
                #print(self.partie.compteur_cases)
        if self.frames > 5 or self.est_au_dessus(self.drone.pos,self.ordre_pos):
            self.tour += 1
            self.frames = 0
        self.frames += 1

    def enleve_gemme(self):
        for elm in self.plateau_de_jeu.walk(loopback=True):
            if self.est_au_dessus(self.drone.pos,elm.pos) and elm.type == "gemme":
                self.plateau_de_jeu.remove_widget(elm)

    def coords_vers_pixels(self,coords):
        x,y = coords
        return (50*x+2,50*y+10)

    def est_au_dessus(self, pos_drone, pos_ordre):
        x_drone, y_drone = pos_drone
        x_ordre, y_ordre = pos_ordre
        if x_ordre - self.precision <= x_drone and x_drone <= x_ordre + self.precision and y_ordre - self.precision <= y_drone and y_drone <= y_ordre + self.precision:
            return True
        return False

    def avance_le_drone(self,x_ordre,y_ordre):
        '''Avance le drone d'une case, met à jour sa position'''
        x_actuel,y_actuel = self.drone.pos
        x_ordre,y_ordre = self.coords_vers_pixels((x_ordre,y_ordre))
        if self.sur_la_meme_ligne(x_actuel,x_ordre):
            if y_actuel < y_ordre:
                self.drone.moveUp()
            if y_actuel > y_ordre:
                self.drone.moveDown()
        elif self.sur_la_meme_colonne(y_actuel,y_ordre):
            if x_actuel < x_ordre:
                self.drone.moveRight()
            if x_actuel > x_ordre:
                self.drone.moveLeft()
        elif x_actuel != x_ordre and y_actuel != y_ordre: # déplacement en diagonale
            if x_actuel > x_ordre and y_actuel < y_ordre: #Diagonale haut-gauche
                self.drone.moveUpLeft()
            elif x_actuel > x_ordre and y_actuel > y_ordre: #Diagonale bas-gauche
                self.drone.moveDownLeft()
            elif x_actuel < x_ordre and y_actuel < y_ordre: #Diagonale haut-droit
                self.drone.moveUpRight()
            elif x_actuel < x_ordre and y_actuel > y_ordre: #Diagonale bas-droit
                self.drone.moveDownRight()

    def sur_la_meme_ligne(self,x_actuel,x_ordre):
        '''Retourne True ou False en fonction de si la cible est sur la même ligne ou non
        les paramètres doivent être en pixels'''
        if  x_ordre - 10 < x_actuel and x_actuel < x_ordre + 10:
            return True
        return False
    
    def sur_la_meme_colonne(self,y_actuel,y_ordre):
        '''Retourne True ou False en fonction de si la cible est sur la même colonne ou non
        les paramètres doivent être en pixels'''
        if  y_ordre - 10 < y_actuel and y_actuel < y_ordre + 5:
            return True
        return False


    def saveDrone(self):
        i = 0
        for elm in self.plateau_de_jeu.walk(loopback=True):
            i += 1
        j = 0
        for elm in self.plateau_de_jeu.walk(loopback=True):
            if j == i-2:  
                self.drone = elm
            j += 1    

    def saveMap(self):
        i = 0
        for elm in self.plateau_de_jeu.walk(loopback=True):
            if i == 1:
                self.map = elm
            i += 1    

    def saveCompteur(self):
        i = 0
        for elm in self.plateau_de_jeu.walk(loopback=True):
            i += 1
        j = 0
        for elm in self.plateau_de_jeu.walk(loopback=True):
            if j == i-1:
                self.compteur = elm
            j += 1  

    def lis_fichier_partie(self):
        '''Lit un fichier de partie pour jouer le replay'''
        self.replay_tours = []
        self.partie.gemmes = []
        gemmes = []
        i = 0
        with open("partie.txt", "r") as fichier_partie:
            for line in fichier_partie:
                if self.test_int(line[:-1]):
                    gemmes.append(int(line[:-1]))
                    
                else:
                    self.replay_tours.append(line)
        for i in range(0,len(gemmes),2):
            self.partie.gemmes.append((gemmes[i],gemmes[i+1]))
        print(self.partie.gemmes)
        

    def test_int(self,chaine):
        try:
            int(chaine)
            return True
        except:
            return False


    def saveTest(self):
        for elm in self.plateau_de_jeu.walk(loopback=True):
            print(elm)


    def build(self):
        self.plateau_de_jeu = plateau_de_jeu()
        self.plateau_de_jeu.gemmes = self.partie.gemmes
        self.plateau_de_jeu.assemble_les_elements()
        self.saveDrone()
        self.saveMap()
        self.saveCompteur()
        self.saveTest()
 
        return self.plateau_de_jeu

simulateurApp().run()