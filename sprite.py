from kivy.graphics import Line, Rectangle, Color
from kivy.uix.widget import Widget
from kivy.core.window import Window

class drone(Widget):
    '''Taille du drone : 45x78'''

    def __init__(self, **kwargs):
        super(drone, self).__init__(**kwargs)
        self.type = "drone"
        self.newDrone()
        self.decalage = 10
    
    def updateDrone(self):
        ''' Synchronise les attributs de position du drone en fonction de la position du sprite'''
        self.pos = self.spriteImg.pos
        self.size = self.spriteImg.size

    def newDrone(self):
        ''' Génère l'affichage d'un nouveau drone'''
        with self.canvas:

            self.spriteImg = Rectangle(pos=(10,10), size=(45,78),source="ressources/Drone/down.png", id="drone1")
            self.size = self.spriteImg.size
            self.updateDrone()
    
    def goTo(self,x,y):
        ''' Positionne le drone aux coordonées reçues'''
        self.spriteImg.pos = (50*x+2,50*y+10)
        self.updateDrone()

    def convertGrid(self):
        #taille : 16x12
        wWidth = Window.size[0]
        wHeight = Window.size[1]
        piece = (wWidth / 16, wHeight / 12)
        print(piece)
        #Recherche (11,9)
        destinationX = 11
        destinationY = 9
        destination = (11*50,9*50)
        print("destination : ", destination)
    
    def moveRight(self):
        with self.canvas:

            self.spriteImg.source = "ressources/Drone/right.png"
            x,y = self.spriteImg.pos
            self.spriteImg.pos = (x+self.decalage,y)
            self.updateDrone()
    
    def moveDown(self):
        with self.canvas:

            self.spriteImg.source = "ressources/Drone/down.png"
            x,y = self.spriteImg.pos
            self.spriteImg.pos = (x,y-self.decalage)
            self.updateDrone()
    
    def moveUp(self):
        with self.canvas:

            self.spriteImg.source = "ressources/Drone/up.png"
            x,y = self.spriteImg.pos
            self.spriteImg.pos = (x,y+self.decalage)
            self.updateDrone()
    
    def moveLeft(self):
        with self.canvas:

            self.spriteImg.source = "ressources/Drone/left.png"
            x,y = self.spriteImg.pos
            self.spriteImg.pos = (x-self.decalage,y)
            self.updateDrone()
    
    def moveUpLeft(self):
        with self.canvas:

            self.spriteImg.source = "ressources/Drone/upleft.png"
            x,y = self.spriteImg.pos
            self.spriteImg.pos = (x-self.decalage,y+self.decalage)
            self.updateDrone()
    
    def moveUpRight(self):
        with self.canvas:

            self.spriteImg.source = "ressources/Drone/upright.png"
            x,y = self.spriteImg.pos
            self.spriteImg.pos = (x+self.decalage,y+self.decalage)
            self.updateDrone()
    
    def moveDownLeft(self):
        with self.canvas:

            self.spriteImg.source = "ressources/Drone/downleft.png"
            x,y = self.spriteImg.pos
            self.spriteImg.pos = (x-self.decalage,y-self.decalage)
            self.updateDrone()
    
    def moveDownRight(self):
        with self.canvas:

            self.spriteImg.source = "ressources/Drone/downright.png"
            x,y = self.spriteImg.pos
            self.spriteImg.pos = (x+self.decalage,y-self.decalage)
            self.updateDrone()

class gemme(Widget):
    """"Définit un nouvel objet gemme"""
    def __init__(self, **kwargs):
        super(gemme, self).__init__(**kwargs)
        self.type = "gemme"
        self.pos = (0,0)
        self.newGemme()
    
    def updateGemme(self):
        "Met à jour les coordonnées et la taille de la gemme en fonction des caractéristique du sprite"
        self.pos = self.spriteImg.pos
        self.size = self.spriteImg.size

    def newGemme(self):
        """Créer les caractéristiques de la nouvelle gemme"""
        with self.canvas:

            self.spriteImg = Rectangle(pos=(0,0), size=(18,18),source="ressources/gem.png")
            self.size = self.spriteImg.size
            self.updateGemme()
    
    def setPos(self,x,y):
        """Change les coordonnées de la gemme"""
        self.spriteImg.pos = 50*x+15,50*y+15
        self.updateGemme()